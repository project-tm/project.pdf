$(function () {
    $("#date").mask("99/99/9999", {placeholder: "дд/мм/гггг"});

    $('.input').each(function (i) {
        var placeholder = $(this).data('title');
        $(this).wrap('<div class="input-wrapper"></div>');
        var style = $(this).val() == '' ? '' : 'input-hideen';
        if ($(this).data('title') == 'E-mail') {
            $(this).parents('.input-wrapper').append('<span class="input-wrapper__text ' + style + '">' + placeholder + ' <span class="black">**</span></span>')
        } else {
            $(this).parents('.input-wrapper').append('<span class="input-wrapper__text ' + style + '">' + placeholder + ($(this).is('.js-register-required') ? ' <span class="red">*</span>' : '') + '</span>')
        }
    });
    $('.input-wrapper__text').click(function () {
        $(this).prev('.input').focus()
    });

    $('#date').focus(function () {
        $(this).next('.input-wrapper__text').hide()
    });
    $('.input').blur(function () {
        if ($(this).val() == '') {
            $(this).next('.input-wrapper__text').show();
        }
    });

    $('.input').keypress(function () {
        $(this).next('.input-wrapper__text').hide()
    })

    $('.js-cyrillic').on('keypress', function (e) {
        if (/[A-Za-z0-9\!\@\#\$\^\&\%\*\(\)\+\=\-\[\]\/\{\}\|\:\<\>\?\,\.]/.test(String.fromCharCode(e.which))) {
            return false;
        }
        return true;
    }).on('paste', function () {
        return false;
    })
    $('.js-cyrillic-tire').on('keypress', function (e) {
        if (/[A-Za-z0-9\!\@\#\$\^\&\%\*\(\)\+\=\[\]\/\{\}\|\:\<\>\?\,\.]/.test(String.fromCharCode(e.which))) {
            return false;
        }
        return true;
    }).on('paste', function () {
        return false;
    })
    $('.js-numeric').on('keypress', function (e) {
        if (/[A-Za-zА-Яа-я\!\@\#\$\^\&\%\*\(\)\+\=\-\[\]\/\{\}\|\:\<\>\?\,\.]/.test(String.fromCharCode(e.which))) {
            return false;
        }
        return true;
    }).on('paste', function () {
        return false;
    })
    $('.js-cyrillic-number').on('keypress', function (e) {
        if (/[A-Za-z\!\@\#\$\^\&\%\*\(\)\+\=\-\[\]\/\{\}\|\:\<\>\?\,\.]/.test(String.fromCharCode(e.which))) {
            return false;
        }
        return true;
    }).on('paste', function () {
        return false;
    })
    $('.js-cyrillic-number-street').on('keypress', function (e) {
        if (/[A-Za-z\!\@\#\$\^\&\%\*\(\)\+\=\[\]\/\{\}\|\:\<\>\?\,\.]/.test(String.fromCharCode(e.which))) {
            return false;
        }
        return true;
    }).on('paste', function () {
        return false;
    })

    var isSex = function () {
        return;
    };

    $('#js-registration-form').submit(function(){     
        $('.js-onclick-disable').prop('disabled',true);
    });
    
});
