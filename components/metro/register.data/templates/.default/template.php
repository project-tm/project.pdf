<form class="auth-form auth-form_width auth-form_offset form-register" id="js-registration-form" action="" method="post">
	<h3 class="auth-form__title">Регистрация
	</h3>
	<h4 class="auth-form__introtext auth-form__introtext_offset">Для получения номера карты Вам необходимо <br>
		заполнить следующую информацию:
	</h4>
	<div class="auth-form__container auth-form__container_left auth-form__container_s-offset ">
        <? if(empty($arParams['isManager'])) { ?>
            <div class="checkbox">
                <label class="checkbox__label">
                    <input type="radio" <?=$arResult['sex_man']?> name="sex" value="man" class="checkbox__input"/>
                    <div class="checkbox__mark"></div>
                    <span class="checkbox__text">Муж.</span>
                </label>
            </div>
            <div class="checkbox">
                <label class="checkbox__label">
                    <input type="radio" <?=$arResult['sex_woman']?> name="sex" value="woman" class="checkbox__input"/>
                    <div class="checkbox__mark"></div>
                    <span class="checkbox__text">Жен.</span>
                </label>
            </div>
            <br/>
        <? } ?>
		<div class="form-item-block">
			<img src='/local/templates/metro/static/img/general/fio.png' alt="fio" class="form-item-icon">
            <input value="<?=$arResult['fname']?>" type="text" name="fname" data-title="Фамилия / Surname" class="input input_small js-name-format js-cyrillic js-register-required"/>
			<input value="<?=$arResult['mname']?>" type="text" name="mname" data-title="Имя / Name" class="input input_small js-name-format js-cyrillic js-register-required"/>
			<input value="<?=$arResult['lname']?>" type="text" name="lname" data-title="Отчество / Middle name" class="input input_small js-name-format js-cyrillic js-register-required"/>
		</div>
        <? if(empty($arParams['isManager'])) { ?>
            <div class="form-item-block">
                <img src='/local/templates/metro/static/img/general/birthday.png' alt="fio" class="form-item-icon">
                <input value="<?=$arResult['birthday']?>" type="text" id="date" name="birthday" data-title="Дата рождения / Birthday" class="input input_small js-register-required"/>
            </div>
        <? } ?>
        <div class="form-item-block">
            <img src='/local/templates/metro/static/img/general/email.png' alt="fio" class="form-item-icon">
            <input value="<?=$arResult['email']?>" type="text" name="email" data-title="E-mail" class="input input_small js-register-required" <? if(!empty($arParams['ID'])) { ?>readonly="readonly"<? } ?>/>
        </div>
        <? if(empty($arParams['isManager'])) { ?>
            <? if($arResult['isCardLoyalty']) { ?>
                <div class="form-item-block">
                    <input value="<?=$arResult['card_loyalty']?>" type="text" name="card_loyalty" data-title="EY Alumni Card Number" class="input input_small js-numeric js-register-required"/>
                </div>
            <? } ?>
            <div class="form-item-block">
                <img src='/local/templates/metro/static/img/general/post.png' alt="fio" class="form-item-icon">
                <input value="<?=$arResult['postcode']?>" type="text" name="postcode" data-title="Почтовый индекс / Postcode" class="input input_small js-name-format js-register-required"/>
                <input value="<?=$arResult['region']?>" type="text" name="region" data-title="Область / Region" class="input input_small js-name-format js-cyrillic"/>
                <input value="<?=$arResult['district']?>" type="text" autocomplete="off" name="district" data-title="Район / District" class="input js-name-format input_small js-cyrillic"/>
                <input value="<?=$arResult['city']?>" type="text" autocomplete="off" name="city" data-title="Город, населенный пункт / City" class="input js-name-format input_small js-cyrillic-tire js-register-required"/>
                <input id="street" value="<?=$arResult['street']?>" type="text" autocomplete="off" name="street" data-title="Улица / Street" class="input input_small js-name-format js-cyrillic-number-street js-register-required"/>
                <br/>
                <input value="<?=$arResult['number-house']?>" type="text" name="number-house" data-title="№ дома / Number house" class="input input_small js-register-required"/>
                <input value="<?=$arResult['housing']?>" type="text" name="housing" data-title="корпус/строение / housing" class="input input_small"/>
                <input value="<?=$arResult['number-flat']?>" type="text" name="number-flat" data-title="№ квартиры / Number flat" class="input input_small"/>
            </div>
        <? } ?>
		<div class="auth-form__text"><span class="red">*</span> Обязательно к заполнению
		</div>
        <? if(empty($arParams['isManager'])) { ?>
            <div class="auth-form__text">** Используется для отправки карты METRO. Обязательно к заполнению
                    </div>
            <div class="checkbox">
                <label class="checkbox__label">
                    <input type="checkbox" name="agree" class="checkbox__input"/>
                    <div class="checkbox__mark"></div>
                    <a href="javascript:void(0)" class="checkbox__text checkbox__text_link js-popup-open">
                        Соглашаюсь с обработкой персональных данных
                    </a>
                </label>
            </div>
            <p></p>
            <div class="checkbox">
                <label class="checkbox__label">
                    <input type="checkbox" name="agree1" class="checkbox__input"/>
                    <div class="checkbox__mark"></div>
                    <span href="javascript:void(0)" class="checkbox__text">
                        Соглашаюсь на получение специального предложения со скидками на первую покупку и получение напоминаний о необходимости получения карты
                    </span>
                </label>
            </div>
        <? } ?>
		<button type="submit" name="register" value="send" class="js-onclick-disable button button_small button_bg-2">Отправить</button>
	</div>
</form>
<? foreach ($arResult['ERRORS_FIELD'] as $error): ?>
	<div class="error_field"><?=$error;?></div>
<? endforeach; ?>
<div class="popup js-popup">
	<div class="popup__modal">
		<div class="popup__title">Пользовательское соглашение
		</div>
		<p class="popup__text">Настоящим я даю свое согласие ООО «МЕТРО Кэш энд Керри» (далее – «МЕТРО») – оператору
			персональных данных, зарегистрированному по адресу: 125445 г.Москва, Ленинградское ш., 71Г, на обработку
			моих персональных данных (далее – «ПД»),
			указанных в настоящей форме, а также иных персональных данных, ставших известными МЕТРО в соответствии с
			требованиями Федерального закона №152-ФЗ от 27.07.2006г. «О персональных данных».
		</p>
		<p class="popup__text">ПД предоставляются для следующих целей: получение карты клиента МЕТРО, для продвижения представленных к продаже товаров, работ, услуг; для проведения клиентских маркетинговых исследований; для дистанционной торговли.
		</p>
		<p class="popup__text">Настоящее согласие предоставляется на осуществление следующих действий в отношении моих
			ПД: сбор, систематизацию, накопление, хранение, обновление, изменение, использование, обезличивание,
			шифрование, блокирование, уничтожение,
			трансграничную передачу.
		</p>
		<p class="popup__text">Срок действия согласия и срок обработки ПД: 3 года с даты подписания настоящей формы;
			указанный срок продлевается в случае заключения договора на период действия договора и 3-летний период после
			его прекращения.
		</p>
		<div class="popup__close js-close">
		</div>
	</div>
</div>
